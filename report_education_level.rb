require 'descriptive_statistics'
require './lib/salary'
require './lib/fix_salary_mistakes'
require './lib/markdown_helper'

class EducationLevelReport < SurveyReport
  include MarkdownHelper

  LEVELS = [
    "High school diploma or GED",
    "Some college",
    "Associates Degree",
    "Bachelors Degree",
    "Some graduate classes",
    "Masters Degree",
    "Doctoral Degree"
    ]

  def run responses
    report_title  "Education Level report"
    question_was Q_EDUCATION

    grouped = responses.group_by {|resp| resp[Q_EDUCATION]}

    section_title "Education Level Counts"
    table_header_row ["Education Level", "Count"]

    LEVELS.each do |level|
      array_to_table [level, grouped[level].size]
    end

    section_title "Percentage of demographics"
    table_header_row ["Education Range", "Percentage"]
    LEVELS.each do |level|
      array_to_table [level, "%2.2f%%" % (100*(grouped[level].size.to_f / responses.size.to_f))]
    end

    section_title "Education level-specific salary statistics"
    LEVELS.map do |level|
      gresponses = grouped[level]
      subsection_title "Salary report for #{level}"
      gresponses = ExcludeUnderMinimumWage.new.fix gresponses
      analysis = SalaryAnalyzer.new gresponses
      stats = analysis.statistics
      puts
      puts AnalyzerDecorator.new(analysis).to_s
      stats
    end
  end
end

REPORTS << EducationLevelReport
