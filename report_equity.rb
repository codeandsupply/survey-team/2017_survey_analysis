class EquityReport < SurveyReport

  def run responses
    report_title "Equity report"
    question_was Q_EQUITY

    grouped = responses.group_by {|resp| resp[Q_EQUITY] }.reject { |resps| resps.size < 1 }

    section_title "Equity Counts"
    table_header_row ["Answer", "Count"]
    grouped.each do |answer,resps|
      array_to_table [answer, grouped[answer].size]
    end

    section_title "Percentage of demographics"
    table_header_row ["Answer", "Percentage"]
    grouped.each do |answer,resps|
      array_to_table [answer, "%2.2f%%" % (100*(grouped[answer].size.to_f / responses.size.to_f))]
    end
  end

end

REPORTS << EquityReport
