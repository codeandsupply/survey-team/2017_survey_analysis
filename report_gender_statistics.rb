require './lib/salary'
require './lib/fix_salary_mistakes'
require 'pp'
require './lib/markdown_helper'


class GenderReport < SurveyReport
  include MarkdownHelper

  def run responses
    analyzer = GenderAnalyzer.new responses
    grouped_by_gender = analyzer.grouped_by_gender
    counts = analyzer.counts

    report_title "Gender report"

    section_title "Counts"
    table_header_row ["Gender category","Count"]
    counts.each do |bucket,count|
      puts "|#{bucket}|#{count.to_i}|"
    end

    section_title "Percentage of demographics"
    table_header_row ["Gender category","Percentage"]
    puts "|male ♂       |%2.2f%%| " % (counts[:male]/counts[:all] * 100)
    puts "|female ♀     |%2.2f%%|" % (counts[:female]/counts[:all] * 100)
    puts "|non-binary ⚧ |%2.2f%%|" % (counts[:non_binary]/counts[:all] * 100)

    subsection_title "Gender-specific salary statistics"
    salary_stats_by_gender = grouped_by_gender.map do |gender, gresponses|
      subsubsection_title "Salary report for #{gender}"
      gresponses = ExcludeUnderMinimumWage.new.fix gresponses
      analysis = SalaryAnalyzer.new gresponses
      stats = analysis.statistics
      puts
      puts AnalyzerDecorator.new(analysis).to_s
      [gender, stats]
    end.to_h

    section_title "Pay gap"
    pay_gap = salary_stats_by_gender[:female][:mean] / salary_stats_by_gender[:male][:mean]
    puts "#{pay_gap} pay gap, meaning that women C&S members are paid #{'%2.2f' % (pay_gap*100)}¢ for every $1.00 earned by C&S's men."

    puts "C&S will conduct more investigation into this figure."

    section_title "Fair compensation feeling"
    puts
    grouped_by_gender_then_by_fair_comp = grouped_by_gender.map do |gender, gresponses|
      [gender, gresponses.group_by { |resp| resp[Q_FAIRLY]} ]
    end.to_h
    table_header_row ["Gender category","Yes","No"]
    grouped_by_gender_then_by_fair_comp.each do |gender,fairlyresp|
      array_to_table [gender,
        "%d (%2.2f%%) " % [fairlyresp["Yes"].size, (fairlyresp["Yes"].size / grouped_by_gender[gender].size.to_f)*100],
        "%d (%2.2f%%) " % [fairlyresp["No"].size, (fairlyresp["No"].size / grouped_by_gender[gender].size.to_f)*100]
      ]
    end

    section_title "Equity per gender"
    grouped_by_gender_then_by_equity = grouped_by_gender.map do |gender, gresponses|
      [gender, gresponses.group_by { |resp| resp[Q_EQUITY]} ]
    end.to_h

    eg = grouped_by_gender_then_by_equity.map do |gender,eqresp|
      eq = eqresp.map do |equity, resps|
        { equity => resps.size }
      end
      { gender => eq }
    end

    puts "\`\`\`"
    require 'yaml'
    puts eg.to_yaml
    puts "\`\`\`"

    puts "_(We will format this sanely sometime.)_"


    {responses_by_gender: grouped_by_gender, salary_stats_by_gender: salary_stats_by_gender}
  end
end

class GenderAnalyzer

  MAPPING = { :male => ['Male', 'M', 'male', 'ftm', 'Maleish', 'm', 'Cis Male', 'Man', 'Maile'],
              :female => ['Female', 'female', 'mtf', 'f', 'F'],
              :non_binary => ['Meat popsicle', 'non-binary', 'nonbinary'],
              :unstated => ['']
            }

  attr_reader :grouped_by_gender

  def initialize responses
    @grouped_by_gender = {}
    @all_size = responses.size
    responses.each do |resp|
      gender = resp[Q_META_GENDER_GROUP]
      unless @grouped_by_gender.has_key? gender
        @grouped_by_gender[gender] = []
      end
      @grouped_by_gender[gender] << resp
    end
  end

  def counts
    known = MAPPING.map { |groups, labels| [groups, grouped_by_gender[groups].size.to_f] }.to_h
    known.merge all: @all_size.to_f
  end
end

REPORTS << GenderReport
