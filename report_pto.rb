require 'descriptive_statistics'
require './lib/salary'
require './lib/fix_salary_mistakes'
require './lib/fix_experience_mistakes'
require './lib/markdown_helper'


class PtoReport < SurveyReport
  include MarkdownHelper

  def run responses
    report_title "PTO report"

    question_was Q_PTO

    puts """A lot of responses had to be interpreted to convert to a number. We
    realized that this question should have been split into three: vacation, sick leave, and holidays.
    Six respondents explicitly stated that they have unlimited sick leave, but others who reported a
    number may also have unlimited sick leave."""

    raw = raw responses
    by_range responses

    section_title "PTO summary"

    puts """With #{raw.statistics[:number].to_i} non-unlimited, interpretable responses, the average PTO of a C&S
    member is approximately #{raw.statistics[:mean].round} days. The least PTO a responder reported
    was #{raw.statistics[:min]} days and the most was #{raw.statistics[:max].to_i} days.
    The highest number of responders have #{raw.statistics[:mode].to_i} days. Two-thirds
    of respondents are between #{(raw.statistics[:mean]-raw.statistics[:standard_deviation]).round}
    and #{(raw.statistics[:mean]+raw.statistics[:standard_deviation]).round} days.
    """
  end

  def raw responses
    section_title "Raw PTO analysis"
    analyzer = PtoAnalyzer.new(responses)
    puts "* #{analyzer.filtered} responses removed because a numerical value could not be inferred."
    puts "* Respondents who reported that they are self-employed were considered to have zero vacation."
    puts "* Responses of 'unlimited' or '0' were not considered for these statistics."
    puts "    * #{analyzer.unlimited_count} responses were unlimited."
    puts
    puts AnalyzerDecorator.new(analyzer).to_s
    analyzer
  end

  RANGES_SPECIAL_CASES = {
    "40+ days" => Proc.new {|number| (40..999).include? number },
    "Unlimited" => Proc.new {|number| (-1.0...0).include? number },
  }

  RANGES = [(0...5), (5...10), (10...15), (15...20), (20...25), (25...30), (30...35), (35...40)].map do |range|
    ["#{range.first} to #{range.last-1} days", Proc.new {|number| range.include? number }]
  end.to_h.merge(RANGES_SPECIAL_CASES)

  def by_range responses
    analyzer = PtoAnalyzer.new(responses)
    grouped = {}

    analyzer.responses.each do |resp|
      bucket = bucket_for resp[Q_PTO]
      unless grouped.has_key? bucket
        grouped[bucket] = []
      end
      grouped[bucket] << resp
    end

    section_title "PTO Counts"
    table_header_row ["PTO Range", "Count"]
    RANGES.keys.each do |range|
      array_to_table [range, grouped[range].size]
    end

    section_title "Percentage of demographics"
    table_header_row ["PTO Range", "Percentage"]
    RANGES.keys.each do |range|
      array_to_table [range, "%2.2f%%" % (100*(grouped[range].size.to_f / responses.size.to_f))]
    end
  end

  def bucket_for number
    RANGES.each do |label, check|
      return label if check.call(number)
    end
  end
end

class PtoAnalyzer
  UNLIMITED = -1.0
  attr_reader :responses

  def filtered
    @initial_count - @filtered_count
  end


  def initialize responses
    @initial_count = responses.size
    @responses = responses.reject { |resp| resp[Q_PTO].nil? || resp[Q_PTO].class != Float }
    @filtered_count = @responses.size
  end

  def statistics
    @responses.map{|resp| resp[Q_PTO]}.reject{|resp| resp <= 0.0 }.descriptive_statistics
  end

  def unlimited_count
    @responses.map{|resp| resp[Q_PTO]}.reject{|resp| resp != -1.0 }.size
  end
end

REPORTS << PtoReport
