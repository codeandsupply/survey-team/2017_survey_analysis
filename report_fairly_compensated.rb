class FairCompensationReport < SurveyReport

  def run responses
    report_title "Fair compensation report"
    question_was Q_FAIRLY

    puts "See the Gender Report for breakdown per gender."

    grouped = responses.group_by {|resp| resp[Q_FAIRLY] }

    section_title "Fair compensation Counts"
    table_header_row ["Answer", "Count"]
    grouped.each do |answer,resps|
      array_to_table [answer, grouped[answer].size]
    end

    section_title "Percentage of demographics"
    table_header_row ["Answer", "Percentage"]
    grouped.each do |answer,resps|
      array_to_table [answer, "%2.2f%%" % (100*(grouped[answer].size.to_f / responses.size.to_f))]
    end
  end

end

REPORTS << FairCompensationReport
