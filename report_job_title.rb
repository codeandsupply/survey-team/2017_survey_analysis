require 'descriptive_statistics'
require './lib/salary'
require './lib/fix_salary_mistakes'
require './lib/fix_job_titles'
require './lib/markdown_helper'

class JobTitleReport < SurveyReport
  include MarkdownHelper

  MINIMUM_NUMBER_FOR_SIGNIFICANCE = 3

  def run responses
    report_title  "Job Title report"
    question_was Q_TITLE

    puts "* This section lists only positions with at least #{MINIMUM_NUMBER_FOR_SIGNIFICANCE} responses."

    fixed_responses = FixJobTitles.new.fix responses

    grouped_maybe_too_few = fixed_responses.group_by {|resp| resp[Q_TITLE] }.sort_by{ |title, resps| resps.size }.reverse.to_h
    grouped = grouped_maybe_too_few.reject{ |title, resps| resps.size < MINIMUM_NUMBER_FOR_SIGNIFICANCE }

    puts "* #{grouped_maybe_too_few.size - grouped.size} results were filtered because of insufficient response count to adequately anonymize data."

    section_title "Job Title Counts"
    table_header_row ["Job Title", "Count"]

    grouped.keys.each do |title|
      array_to_table [title, grouped[title].size]
    end

    section_title "Percentage of demographics"
    table_header_row ["Education Range", "Percentage"]
    grouped.keys.each do |title|
      array_to_table [title, "%2.2f%%" % (100*(grouped[title].size.to_f / fixed_responses.size.to_f))]
    end

    section_title "Job Title-specific salary statistics"
    grouped.keys.map do |title|
      gresponses = grouped[title]
      subsection_title "Salary report for #{title}"
      gresponses = ExcludeUnderMinimumWage.new.fix gresponses
      analysis = SalaryAnalyzer.new gresponses
      stats = analysis.statistics
      puts
      puts AnalyzerDecorator.new(analysis).to_s
      stats
    end
  end
end

REPORTS << JobTitleReport
