class OncallDutyReport < SurveyReport

  def run responses
    report_title "On-call Duty report"
    question_was Q_ONCALL

    grouped = responses.group_by {|resp| resp[Q_ONCALL] }.reject { |resps| resps.size < 1 }

    section_title "On-call Duty Counts"
    table_header_row ["Answer", "Count"]
    grouped.each do |answer,resps|
      array_to_table [answer, grouped[answer].size]
    end

    section_title "Percentage of demographics"
    table_header_row ["Answer", "Percentage"]
    grouped.each do |answer,resps|
      array_to_table [answer, "%2.2f%%" % (100*(grouped[answer].size.to_f / responses.size.to_f))]
    end
  end

end

REPORTS << OncallDutyReport
