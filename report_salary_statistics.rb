require 'descriptive_statistics'
require './lib/salary'
require './lib/fix_salary_mistakes'
require './lib/markdown_helper'


class SalaryStatisticsReport < SurveyReport
  include MarkdownHelper

  def run responses
    report_title "Salary report"
    section_title "Salary statistics"
    puts "* #{responses.size} responses reported in set"
    to_analyze = ExcludeUnderMinimumWage.new.fix responses
    puts "* #{to_analyze.size} salaries to analyze"
    puts
    analysis = SalaryAnalyzer.new to_analyze
    puts AnalyzerDecorator.new(analysis).to_s
  end
end

REPORTS << SalaryStatisticsReport
