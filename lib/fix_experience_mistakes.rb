class FixExperienceMistakes < SurveyFix
  # must return the modified responses
  def fix responses
    fix_count = 0
    fixed_responses = responses.map { |resp|
      resp_exp = resp[Q_EXPERIENCE]
      if resp_exp.class == Fixnum
        resp[Q_EXPERIENCE] = resp_exp.to_f
      elsif resp_exp.class == Float
        resp[Q_EXPERIENCE] = resp[Q_EXPERIENCE]
      elsif resp_exp.class == String
        fixed = fix_string resp_exp
        if fixed.class != Float
          STDERR.puts "Response could not be converted to float: [#{fixed}] which isa #{fixed.class}"
          nil
        end
        resp[Q_EXPERIENCE] = fixed
        fix_count += 1
      end
      resp
    }
    puts "* #{fix_count} experience entries were normalized.\n\n"
    fixed_responses
  end

  ONE_OFF_TABLE = {
    "15+" => 15.0,
    "<1 year" => 1.0,
    "Off & on 33 years" => 33.0,
    "3 and a half" => 3.5,
    "2/3" => 0.6,
    "3 years 6 months" => 3.5,
    "< 1 year" => 1.0,
  }

  def fix_string resp
    years_match = /^(\d\d?|\d.\d) [Y|y]ears?$/.match resp
    unless years_match.nil?
      years = years_match[1].to_f
      STDERR.puts "YEARS FIX: #{resp} -> #{years}"
      return years
    end

    months_match = /^(\d\d?|\d.\d) [M|m]onths?$/.match resp
    unless months_match.nil?
      months = months_match[1].to_f / 12.0
      STDERR.puts "MONTHS FIX: #{resp} -> #{months}"
      return months
    end

    if ONE_OFF_TABLE.member? resp
      result = ONE_OFF_TABLE[resp]
      STDERR.puts "LOOKUP FIX: #{resp} -> #{result}"
      return result
    end

    resp
  end
end

FIXES << FixExperienceMistakes
