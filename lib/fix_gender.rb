Q_META_GENDER_GROUP = :gender_group
class FixGender < SurveyFix

  BUCKETS = GenderAnalyzer::MAPPING.invert


  def bucket_for gender
    bucket = BUCKETS.find { |bucket| bucket[0].member? gender.strip }
    if bucket.nil?
      puts "No bucket for [#{gender}]. Add it!"
      exit 2
    end
    bucket[1]
  end

  def fix responses
    puts "* Gender responsses were grouped."
    BUCKETS.each do |answers, bucket|
      puts "    * #{bucket} answers included responses #{answers.map{|a| '[' + a + ']'}.join(', ')}"
    end
    responses.map do |resp|
      resp[Q_META_GENDER_GROUP] = bucket_for resp[Q_GENDER]
      resp
    end
  end


end

FIXES << FixGender
