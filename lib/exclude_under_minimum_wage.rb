class ExcludeUnderMinimumWage < SurveyFix
  #$7.25 * 40 hours * 52
  MINIMUM_WAGE = 7.25 * 40 * 52
  def fix responses
    to_analyze = responses
    remaining = to_analyze.reject { |r| r[Q_SALARY] < MINIMUM_WAGE }
    rejected_count = to_analyze.size - remaining.size
    unless rejected_count == 0
      puts "* #{rejected_count} rejected for being under minimum wage: "
      require 'pp'
      print "     ```\n        "
      PP.pp (to_analyze - remaining).group_by { |r| r[Q_SALARY] }.map { |group, entries| [group, entries.size ] }.to_h
      puts "     ```"
    end
    remaining
  end
end
