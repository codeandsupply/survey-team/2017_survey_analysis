class RolesReport < SurveyReport

  def run responses
    analyzer = RolesAnalyzer.new responses

    report_title "Roles report"
    question_was Q_ROLE

    puts """This question allowed multiple answers, so counts will exceed total
    number of responses. The goal here is to show how many of respondents
    reported activity in the given role."""

    puts "* #{analyzer.removed} responses were not considered because no role was chosen."
    puts "* #{analyzer.dropped} categories were dropped because there were not enough responses for them to be statistically significant."

    section_title "Roles Counts"
    table_header_row ["Answer", "Count"]
    analyzer.grouped.each do |answer,resps|
      array_to_table [answer, analyzer.grouped[answer].size]
    end

    section_title "Percentage of demographics"
    table_header_row ["Answer", "Percentage"]
    analyzer.grouped.each do |answer,resps|
      array_to_table [answer, "%2.2f%%" % (100*(analyzer.grouped[answer].size.to_f / responses.size.to_f))]
    end

    section_title "Role-specific salary statistics"

    puts "Keep in mind that this should be read as \"If I am doing this role, these statistics may apply to me.\""

    analyzer.grouped.map do |answer, gresponses|
      subsection_title "Salary report for #{answer}"
      gresponses = ExcludeUnderMinimumWage.new.fix gresponses
      analysis = SalaryAnalyzer.new gresponses
      stats = analysis.statistics
      puts
      puts AnalyzerDecorator.new(analysis).to_s
      stats
    end

  end
end

REPORTS << RolesReport

class RolesAnalyzer
  MINIMUM_NUMBER_FOR_SIGNIFICANCE = 3
  attr_reader :grouped, :removed, :dropped

  def explode_roles resp
    resp[Q_ROLE].split(";").map{|role| make_uniform(role) }
  end

  def make_uniform role
      RolesCategorizer.rename role
  end

  def initialize responses
    @responses = responses
    @removed = 0
    @dropped = 0
    @grouped = {}

    responses.each do |resp|
      unless resp[Q_ROLE].nil? || resp[Q_ROLE].empty?
        explode_roles(resp).each do |role|
          unless @grouped.has_key? role
            @grouped[role] = []
          end
          @grouped[role] << resp
        end
      else
        @removed += 1
      end
    end

    @grouped = @grouped.sort_by{|answer, resps| resps.size }.reverse.to_h
    before_filter_count = @grouped.size
    @grouped = @grouped.reject {|answer, resps| resps.size <= MINIMUM_NUMBER_FOR_SIGNIFICANCE }
    @dropped = before_filter_count - @grouped.size

  end
end

class RolesCategorizer
   NO_RENAME = [
     "Back-end web development",
     "Front-end web development",
     "Systems engineering",
     "Data Science",
     "Management",
     "UI Design",
     "UX Design",
     "Recruitment",
     "Quality Engineering",
     "DevOps",
     "Sales Engineering",
     "Applied R&D",
     "Robotics Engineering",
     "Embedded Development",
     "Education",
     "Project Management",
     "Evangelism"
   ]

   RENAME = {
     "DevOps" => [ "Dev-ops" ],
     "Quality Engineering" => [ "Quality Engneering" ],
     "Technical Support" => [ "Support", "Systems Support", "Technical Support Engineer" ],
     "Mobile Development" => [ "Mobile development", "App Engineering (Mobile)", "Mobile Engineering", "Native application development (iOS and macOS)", "Mobile Developer" ],
     "Technical Writing" => [ "technical writing" ],
     "Systems engineering" => [ "Non-Web development, which still exists", "Desktop Application Development", "Native apps" , "Non Web Software Development", "Software Development", "Non-web Software Engineering", "Desktop Applications", "Software development" ],
     "Database Administration and Development" => [ "Database", "Discovery database", "Database dev", "Database Development" ],
     "Site Reliability Engineering" => [ "Reliability engineering" ],
     "Robotics Engineering" => ["Robotics Software Engineer" ],
     "Management" => [ "Contracts Manager", '"Team Anchor"(scrum master)' ],
     "Data Science" => [ "analytics" ],
     "Embedded Development" => ["Firmware development", "Embedded Software", "hardware design/ microcontroller firmware"],
     "Project Management" => ['Project Manager'],
     "Evangelism" => ["Business Development, Evangelism"],
     "Architecture" => ["Infrastructure/architect"],
     "High Performance Computing" => [ "Computational Science/HPC" ],
   }.invert.map{|entries,use| entries.map { |entry| {entry => use} } }.flatten # it's too late the <center> cannot hold

   def self.rename given
     return given if NO_RENAME.member? given
     rename = RENAME.find {|pair| pair[given] }
     unless rename.nil?
       return rename.values.first
     end
     given
   end

end
